﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceService.Models
{

    public class Attachment
    {
        public string fileName { get; set; }
        public string attachmentBody { get; set; }
    }

    public class Attachments
    {
        public bool Success { get; set; }
        public string errorMessage { get; set; }
        public List<Attachment> attachments { get; set; }
    }
}
