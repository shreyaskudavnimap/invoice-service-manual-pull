﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceService.Models
{
    public class BrokeragePaymentMilestone
    {
        public string bookingId { get; set; }
        public string poNumber { get; set; }
        public string brokerageType { get; set; }
        public double brokerageDue { get; set; }
        public bool active { get; set; }
        public string chequeNo { get; set; }
        public double brokerageDuePercentage { get; set; }
        public bool dueForPayment { get; set; }
        public string dueDate { get; set; }
        public double amountPaidByCustomerPercentage { get; set; }
        public bool registrationDateCriteriaApplicable { get; set; }
        public double amountPaidByCustomer { get; set; }
        public string invoiceNumber { get; set; }
        public DateTime lastModifiedDate { get; set; }
        public string attachmentId { get; set; }
        public string SESNumber { get; set; }
        public string bpm18digitId { get; set; }
        public DateTime createdDate { get; set; }
        public string bpmTaxCode { get; set; }
    }

    public class Invoices
    {
        public bool success { get; set; }
        public string errorMessage { get; set; }
        public List<BrokeragePaymentMilestone> brokeragePaymentMilestones { get; set; }
    }
}
