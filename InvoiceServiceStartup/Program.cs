﻿using InvoiceService;
using InvoiceService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;

namespace InvoiceServiceStartup
{
    public class Program
    {
        static DbCommand _sqlcommand = null;
        static DataAccess dataAccess = new DataAccess();
        static string LOG_FILE = "log.txt";
        static string apiUrl = ConfigurationManager.AppSettings["SFDCURL"] + "/services/apexrest/api/brokeragePaymentMilestones";

        static void Main(string[] args)
        {
            var config = GetConfig();
            var startDatetime = config.FromDate;        //"2021-04-10 00:00:00"; //DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            var endDatetime = config.ToDate;             //"2021-04-01 00:00:00"; //Convert.ToDateTime(endDatetime).Add(new TimeSpan(-500, 0, 0)).ToString("yyyy-MM-dd 00:00:00");       //last 15 min 

            //var currentDatetime = "2021-04-23 12:45:00";        //DateTime.Now.ToString("yyyy -MM-dd HH:mm");
            //var startDatetime = Convert.ToDateTime(currentDatetime).Add(new TimeSpan(0, -15, 0)).ToString("yyyy-MM-dd HH:mm:ss");       //last 15 min 

            Body body = new Body();
            body.fromDate = startDatetime;
            body.toDate = endDatetime;
            var bodyString = JsonConvert.SerializeObject(body);
            int counter = 1;
            try
            {
                Console.WriteLine("Started...");
                string response = sendRequest(bodyString, apiUrl);
                Console.WriteLine("Data fetched from API");

                Invoices lst = Newtonsoft.Json.JsonConvert.DeserializeObject<Invoices>(response);
                Console.WriteLine("Data transformed");

                foreach (var inv in lst.brokeragePaymentMilestones)
                {

                    dataAccess.GetDataSet("usp_Invoice", ref _sqlcommand,
                    "@BookingId", SqlDbType.VarChar, inv.bookingId, ParameterDirection.Input,
                    "@PoNumber", SqlDbType.VarChar, inv.poNumber, ParameterDirection.Input,
                    "@BrokerageType", SqlDbType.VarChar, inv.brokerageType, ParameterDirection.Input,
                    "@BrokerageDue", SqlDbType.VarChar, inv.brokerageDue, ParameterDirection.Input,
                    "@Active", SqlDbType.VarChar, inv.active, ParameterDirection.Input,
                    "@ChequeNo", SqlDbType.VarChar, inv.chequeNo, ParameterDirection.Input,
                    "@BrokerageDuePercentage", SqlDbType.VarChar, inv.brokerageDuePercentage, ParameterDirection.Input,
                    "@DueForPayment", SqlDbType.VarChar, inv.dueForPayment, ParameterDirection.Input,
                    "@DueDate", SqlDbType.VarChar, inv.dueDate, ParameterDirection.Input,
                    "@AmountPaidByCustomerPercentage", SqlDbType.VarChar, inv.amountPaidByCustomerPercentage, ParameterDirection.Input,
                    "@SESNumber", SqlDbType.VarChar, inv.SESNumber, ParameterDirection.Input,
                    "@RegistrationDateCriteriaApplicable", SqlDbType.VarChar, inv.registrationDateCriteriaApplicable, ParameterDirection.Input,
                    "@AmountPaidByCustomer", SqlDbType.VarChar, inv.amountPaidByCustomer, ParameterDirection.Input,
                    "@InvoiceNumber", SqlDbType.VarChar, inv.invoiceNumber, ParameterDirection.Input,
                    "@LastModifiedDate", SqlDbType.VarChar, inv.lastModifiedDate.ToString("yyyy-MM-dd HH:mm:ss"), ParameterDirection.Input,
                    "@AttachmentId", SqlDbType.VarChar, inv.attachmentId, ParameterDirection.Input,
                    "@bpm18digitId", SqlDbType.VarChar, inv.bpm18digitId, ParameterDirection.Input,
                    "@createdDate", SqlDbType.VarChar, inv.createdDate.ToString("yyyy-MM-dd HH:mm:ss"), ParameterDirection.Input,
                    "@flag", SqlDbType.VarChar, "UPDATE_INV", ParameterDirection.Input,
                    "@bpmTaxCode", SqlDbType.VarChar, inv.bpmTaxCode, ParameterDirection.Input);

                    if (inv.attachmentId != "")
                    {
                        using (var client = new WebClient())
                        {
                            var response1 = client.DownloadString("https://d4u1.gplapps.com:8085/HerokuCommonAPI/getAttachmentDocuments?attachmentId=" + inv.attachmentId);
                            Attachments attachlst = Newtonsoft.Json.JsonConvert.DeserializeObject<Attachments>(response1);
                            if (attachlst.attachments.Count != 0)
                            {
                                dataAccess.GetDataSet("usp_Invoice", ref _sqlcommand,
                                    "@BookingId", SqlDbType.VarChar, inv.bookingId, ParameterDirection.Input,
                                    "@PoNumber", SqlDbType.VarChar, inv.poNumber, ParameterDirection.Input,
                                    "@bpm18digitId", SqlDbType.VarChar, inv.bpm18digitId, ParameterDirection.Input,
                                    "@AttachmentId", SqlDbType.VarChar, inv.attachmentId, ParameterDirection.Input,
                                    "@AttachmentFileName", SqlDbType.VarChar, attachlst.attachments[0].fileName, ParameterDirection.Input,
                                    "@AttachmentBody", SqlDbType.VarChar, attachlst.attachments[0].attachmentBody, ParameterDirection.Input,
                                    "@flag", SqlDbType.VarChar, "UPDATE_ATTC", ParameterDirection.Input);
                            }
                        }
                    }
                    //Console.WriteLine("Rows : " + counter);
                    Console.Write("\r{0}/{1} inserted", counter, lst.brokeragePaymentMilestones.Count);

                    counter += 1;
                }

            }
            catch (Exception ex)
            {
                Log("Error", ex.Message + "\tStack error:" + ex.StackTrace);
            }
        }

        public static string sendRequest(string body, string url)
        {
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = "Bearer " + getToken();

                var response = client.UploadString(url, "POST", body);
                return response;
            }
        }
        public static void Log(string type, string message)
        {
            string path = Environment.CurrentDirectory;
            using (StreamWriter sw = File.AppendText(path + "\\" + LOG_FILE))
            {
                sw.WriteLine("[" + DateTime.Now + "][" + type + "]\t\t" + message);
            }
        }
        public static string getToken()
        {
            using (WebClient client = new WebClient())
            {
                var reqparm = new System.Collections.Specialized.NameValueCollection();
                reqparm.Add("param1", "");
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                Uri myUri = new Uri(ConfigurationManager.AppSettings["SFDCURL"] + ConfigurationManager.AppSettings["GRANTSERVICE"] + "&client_id="
                    + ConfigurationManager.AppSettings["CLIENTID"] + "&client_secret=" + ConfigurationManager.AppSettings["CLIENTSECRET"] + "&username="
                    + ConfigurationManager.AppSettings["USERNAME"] + "&password=" + ConfigurationManager.AppSettings["Password"], UriKind.Absolute);

                //Uri myUri = new Uri("https://godrej.my.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9Y6d_Btp4xp7lt5FL02Cc.bHBCI_vpcrJYvpeBev1Ob5nXpobkxkmhygUekoOvbQMscya0i3r7EacuDWz&client_secret=332639414523432900&username=external.app@godrejproperties.com&password=Nov@12345Svgw3cZob0A7HWlJs8I07Jcdb", UriKind.Absolute);

                byte[] responsebytes = client.UploadValues(myUri, reqparm);

                string responseBody = System.Text.Encoding.UTF8.GetString(responsebytes);
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                var result = jsSerializer.DeserializeObject(responseBody);
                Dictionary<string, dynamic> obj2 = new Dictionary<string, dynamic>();
                obj2 = (Dictionary<string, dynamic>)(result);
                return obj2["access_token"].ToString();
            }
        }

        public static Config GetConfig()
        {
            string filename = "settings.xml";
            XmlDocument xdc = new XmlDocument();
            xdc.Load(filename);
            XmlNodeList xnlNodes = xdc.SelectNodes("settings");
            Config conf = new Config();

            foreach (XmlNode xnlNode in xnlNodes)
            {
                XmlElement element = (XmlElement)xnlNode;

                conf.FromDate = Convert.ToString(xnlNode["From"].InnerText);
                conf.ToDate = Convert.ToString(xnlNode["To"].InnerText);
            }
            return conf;
        }

        public class Body
        {
            public string fromDate { get; set; }
            public string toDate { get; set; }
        }

        public class Config
        {
            public string FromDate { get; set; }
            public string ToDate { get; set; }
        }
    }
}
