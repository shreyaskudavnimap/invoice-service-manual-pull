﻿
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceService
{
    public class DataAccess
    {
        private SqlDatabase sqlDatabase = null;


        /// <summary>
        /// Return a DataSet populated by executing the specified stored procedure. this method takes directly the SQLParameter collection
        /// and should be used when a store procedure is expected to return a output parameter
        /// </summary>
        /// <param name="storedProcedureName">Name of the stored procedure to execute</param>
        /// <param name="sqlParameter">SQL Parameter collection</param>
        /// <returns>Returns a dataset populated by executing the store proc</returns>
        public DataSet GetDataSet(string storeProcedureName, ref DbCommand dbCommand, params object[] nameTypeValue)
        {
            using (dbCommand = GetStoreProcCommand(storeProcedureName, nameTypeValue))
            {
                DataSet retval;
                try
                {
                    retval = sqlDatabase.ExecuteDataSet(dbCommand);
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    if (dbCommand.Connection.State == ConnectionState.Open)
                    {
                        dbCommand.Connection.Close();
                        dbCommand.Connection.Dispose();
                    }
                }
                return retval;
            }
        }


        private DbCommand GetStoreProcCommand(string storedProcedureName, object[] nameTypeValue)
        {
            //sqlDatabase = new SqlDatabase(GetConnectionString());
            sqlDatabase = new SqlDatabase(GetConnectionString());
            DbCommand dbCommand = sqlDatabase.GetStoredProcCommand(storedProcedureName);
            dbCommand.CommandTimeout = 0;
            BuildSqlParameterArray(sqlDatabase, ref dbCommand, nameTypeValue);
            return dbCommand;
        }

        private void BuildSqlParameterArray(SqlDatabase sqlDatabase, ref DbCommand dbCommand, params object[] nameTypeValue)
        {
            //if ((nameTypeValue.Length % 4) != 0)
            //    throw new Exception("Name/Type/Value/ParameterDirection is unbalanced");

            int argCount = nameTypeValue.Length / 4;

            for (int arg = 0, ntv = 0; arg < argCount; ++arg, ntv += 4)
            {
                sqlDatabase.AddParameter(dbCommand, (string)nameTypeValue[ntv], (SqlDbType)nameTypeValue[ntv + 1], (ParameterDirection)nameTypeValue[ntv + 3], String.Empty, DataRowVersion.Default, nameTypeValue[ntv + 2]);
            }
        }

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["godrejcpmsConnectionString"].ConnectionString;
        }
    }
}
